package domain;

import java.io.*;
import static java.lang.Math.abs;
import java.util.*;

public class Solution {

    // Complete the flatlandSpaceStations function below.
    static int flatlandSpaceStations(int n, int[] c) {
        int guardar = 0;
        int distancia = 9999;
        for (int i = 0; i < n; i++) { //recorre ciudad por ciudad
            for (int j = 0; j < c.length; j++) { //estando en la ciudad verifica la distancia a todas las estaciones
                if (abs(c[j] - i) < distancia) {
                    distancia = abs(c[j] - i);          //obtiene la distancia mas pequenia a la estacion
                }
            }
            if (distancia>guardar) {//saca el mayor de las distancias mas pequenias
                guardar = distancia;
            }

            distancia = 9999; //formatea la distancia para volver a verificar
        }

        return guardar;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String[] nm = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nm[0]);

        int m = Integer.parseInt(nm[1]);

        int[] c = new int[m];

        String[] cItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < m; i++) {
            int cItem = Integer.parseInt(cItems[i]);
            c[i] = cItem;
        }

        int result = flatlandSpaceStations(n, c);

        System.out.println(result);

        scanner.close();
    }
}
